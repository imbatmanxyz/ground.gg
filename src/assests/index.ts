import Logo from "./logo.svg"
import Lol from "./lol.jpg"
import Dota from "./daota2.jpg"
import Fortnite from "./fortnite.jpg"
import CsGo from "./cs_go_two.jpg"
import Hearthstone from "./hearthstone.png"
import Overwatch from "./overwatch.jpg"
import TwoOne from "./21.png"
import Hippo from "./hippo.png"
import Jerry from "./jerry.png"
import Rick from "./rick-icon.png"
import Stewie from "./stewie.jpg"
import Persona from "./grounds-gg-persona.svg"
import Graph from "./graph.svg"


export default {
    logo: Logo, Lol, Dota, Fortnite, CsGo, Hearthstone, Overwatch, TwoOne, Hippo, Jerry, Rick, Stewie, Persona, Graph
}
import React from 'react';
import styled from 'styled-components';
import { Sub, Title } from '../themes/typography';
import Images from '../assests';



const RewardInfo = () => {
  const [displayReward, setDisplayReward] = React.useState(false);

  const Wrapper = styled.div`
    position: absolute;
    color: black;
    z-index: 1;
    right: 196px;
    height: 100px;
    width: 100px;
  `;

  const Info = styled.div<{ displayReward: boolean; }> `
    ${props => props.displayReward ? `display: block;` : `display: none;`}
    height: fit-content;
    width: fit-content;
    background-color: #F44B44;
    padding: 16px;
    border-radius: 8px;
    color: #FFFFF2;
  `;

  return <Wrapper
    onMouseEnter={() => setDisplayReward(!displayReward)}
    onMouseLeave={() => setDisplayReward(!displayReward)}
  >
    <Info displayReward={displayReward}>
      <h3>Rewards Unlocked</h3>
    </Info>
  </Wrapper>;
};


export function Reward() {
  const Container = styled.article`
    grid-area: crypto;
    margin-top: 256px;
    background-color: #F44B44;
    grid-column: 1/13;
    display: flex;
    justify-content: space-between;
    height: 400px;
    width: 100%;
    color: white;
    padding-left: 48px;
    color: #FFFFF2;
    &> div>  p, li {
      font-weight: 400;
      font-size: 20px;
      color: #FFFFF2
    }
  `;

  const RewardGraph = styled.div`
    width: 50%;
    background-color: #FFFFF2;
    &> img {
      margin-top: 24px;
      margin-left: 48px;
      height: 400px;
    }
  `;

  return <Container>
    <div>
      <Title>Earn rewards</Title>
      <Sub>Complete your challenges, build your persona, and earn rewards</Sub>
      <p>Use crypto on the platform to buy:</p>
      <ul>
          <li>hardware</li>
          <li>software</li>
          <li>in-game content</li>
          <li>gamer merch</li>
          <li>and more...</li>
      </ul>
    </div>
    <RewardGraph>
      <RewardInfo />
      <img src={Images.Graph} />
    </RewardGraph>
  </Container>
}
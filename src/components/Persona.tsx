import React from "react";
import styled from 'styled-components';
import { Title, Sub } from "../themes/typography";
import Images from "../assests"


export function Persona() {
  const Container = styled.article`
    grid-area: persona;
    grid-column: 1/13;
    display: flex;
    flex-direction: column;
    margin-top: 64px;
  `;

  const Content = styled.div`
    position: relative;
    left: 0;
    height: 364px;
    width: 50%;
    margin-top: 48px;
    color: #FFFFF2;
    background-color: #F44B44;
    z-index: 1;
    padding: 24px 48px;
    &> button {
      padding: 16px;
      border-radius: 8px;
      outline: none;
      border: none;
      margin-top: 24px;
      background-color: #F0F0E4;
      color: #232323;
      font-weight: bold;
      font-size: 16px;
    }
    &>button:hover {
      cursor: pointer;
      padding: 16px;
      border-radius: 8px;
      outline: none;
      border: none;
      margin-top: 24px;
      background-color: #F44B44;
      color: #FFFFF2;
      font-weight: bold;
      font-size: 16px;
    }
    &> p {
      font-weight: 400;
      font-size: 20px;
      color: #FFFFF2
    }
  `;

  const Image = () => {
    const Wrapper = styled.div`
      position: absolute;
      right: 0;
      height: 500px;
      width: 50%;
      z-index: -1;
      &> img {
        height: 100%;
        width: 100%;
      }
    `;

    return <Wrapper>
      <img src={Images.Persona} />
    </Wrapper>
  }

  return <Container>
    <Content>
      <Title>Build a persona</Title>
      <Sub>Show off your stats, badges, and trophies</Sub>
      <p>Use your in-game profile to create a Grounds Persona. Your Persona will follow you as you level up your account, complete challenges, and earn rewards.</p>
      <button>Build persona</button>
    </Content>
    <Image />
  </Container>
}
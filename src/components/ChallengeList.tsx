import React from 'react';
import styled from 'styled-components';
import Images from '../assests/index';
import { Env } from '../context';
import { Sub, Title } from '../themes/typography';


const Container = styled.article`
  grid-area: challenges;
  margin-top: 196px;
  grid-column: 1/13;
  display: flex;
  justify-content: space-evenly;
  height: 400px;
  width: 100%;
  padding-top: 8px;
  &> p {
    font-size: 16px;
  }
`

const ChallengeBlock = styled.div`
  &> p {
    font-weight: 400;
    font-size: 20px;
    color: rgb(140, 140, 140)
  }
`

const List = () => {
  const Container = styled.div``;
  const ListTitle = styled(Title)`
    border-bottom: 16px solid #F0F0E4;
    &> span {
      font-style: italic;
      font-size: 24px;
      margin-right: 8px;
      color: rgb(204, 204, 204);
    }
  `;

  const Item = styled.div`
    display: flex;
    align-content: center;
    margin-top: 8px;
    &> img {
      margin-right: 16px;
      height: 48px;
      width: 48px;
    }
    &> p {
      font-size: 20px;
      font-weight: 400;
      color: rgb(140, 140, 140)
    }
  `;

  const Btn = styled.button`
    padding: 16px;
    border-radius: 8px;
    outline: none;
    border: none;
    margin-top: 24px;
    background-color: #F0F0E4;
    color: #232323;
    font-weight: bold;
    font-size: 16px;
    &:hover {
      cursor: pointer;
      background-color: #232323;
      color: #FFFFF2;
    }
  `;

  return <Container>
    <ListTitle><span>some</span>Challenges</ListTitle>
    {[{
      "challenge": "Hungry, hungry hippo",
      "icon": Images.Hippo
    }, {
      "challenge": "Never gonna die",
      "icon": Images.Stewie
    }, {
      "challenge": "21 Jump street",
      "icon": Images.TwoOne
    }, {
      "challenge": "More Rick than Morty",
      "icon": Images.Rick
    }, {
      "challenge": "Being a Jerry",
      "icon": Images.Jerry
    }].map(({ challenge, icon }: { challenge: string; icon: string; }, index: number) => {
      return <Item>
        <img src={icon} />
        <p>{challenge}</p>
      </Item>;
    })}
    <Btn>View more challenges</Btn>
  </Container>;
};


export function Challenge({ context }: { context: Env }) {
  return <Container>
    <ChallengeBlock>
      <Title>Compete in challenges</Title>
      <Sub>Complete challenges, earn rewards, get better at your game</Sub>
      <br />
      <p>Custom challenges will be created when you sign up</p>
      <p>Grounds will anaylse your game history and create challenges based on your gamestyle</p>
      <p>Majority of the challenges are there to help you get better at the game</p>
      <p>but we all like a little bit of fun...</p>
      <p>Some challenges can earn your a lot of rewards but are very difficult</p>
    </ChallengeBlock>
    <List />
  </Container>
}
import React from 'react';
import styled from 'styled-components';
import { Env } from '../context';
import { Logo } from './Logo';
import { Search } from './Search';
import { Download } from './Download';


export function Header({ context }: { context: Env }) {
  const Container = styled.header`
    grid-area: header;
    padding-top: 128px;
    grid-column: 4 / 10;
    display: flex;
    flex-direction: column;
    align-items: center;
  `;

  return <Container>
    <Logo large />
    <Search />
    <Download />
  </Container>
}

// import { Link, useNavigate } from 'react-router-dom';
// import { Env } from '../context';
// import { RegionSelect } from './RegionSelect';



// function renderErrorMessage(error: number) {
//   const errors: {[key: number]: string} = {
//     202: "account exists",
//     500: "try again"
//   };

//   return errors[error]
// }

// const Btn = styled.button``
//   // background: 'linear-gradient(45deg, #16BA97 10%, #5AC7AF 90%)',
//   // color: '#FFFFF2',
//   // fontWeight: "bold",
//   // fontSize: "16px",
//   // marginTop: "16px",
//   // padding: "16px",
//   // borderRadius: "0.5em",

// const Input = styled.input`
//   // background: #FFFFF2;
//   // border-radius: 0.5em;
//   // border: none;
//   // padding: 0.8em;
//   // margin-top: 8px;
    
//   // color: #40403D;
//   // font-size: 1.1em;
//   // padding-left: 1.5em;
    
//   // outline: none;
//   // &::placeholder {
//   //   font-family: Signika;
//   //   font-weight: 400;
//   // }
// `;

// export const FormValidate = ({ context }: { context: Env; }) => {
//   let navigate = useNavigate();

//   let [error, setError] = React.useState<number>(0);
//   let [region, setRegion] = React.useState<string>("euw1");

//   let summonerNameRef = React.useRef<HTMLInputElement>(null);
//   console.log(summonerNameRef.current?.value);

//   async function handleSubmission(evt: React.FormEvent<HTMLFormElement>) {
//     evt.preventDefault();

//     const res = await fetch(`${context.urlBase}createProfile`, {
//       method: "POST",
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         region: region,
//         summonerName: encodeURIComponent(summonerNameRef.current!.value)
//       })
//     });

//     if (res.status === 201) {
//       const profileId = await res.text();
//       navigate(`analyse/${profileId}`, { replace: true });
//     } else {
//       setError(res.status);
//     }
//   }

//   return <div style={{
//     height: "100%",
//     width: "100%",
//     overflow: "hidden",
//     display: "block"
// }}>
//   <div style={{
//     maxWidth: "1920px",
//     margin: "0px auto",
//     padding: "90px 0px 40px",
//     position: "relative",
//     width: "100%",
//     background: "none",
//     display: "flex",
//     flexDirection: "column",
//     justifyContent: "center",
//     alignItems: "center",
//     minHeight: "auto",
//     boxSizing: "border-box"
// }}>
//   <div style={{
//     position: "relative",
//     width: "83%",
//     maxWidth: "1920px",
//     margin: "0px auto",
//     display: "block"
// }}>
//     <form className="form" style={{display: "block"}} onSubmit={handleSubmission}>
//       {/* <h1>Gamify your way to rewards and victory</h1> */}
//       <div style={{
//     maxWidth: "320px",
//     minHeight: "92px",
//     margin: "0px auto"
// }}>
//       <div style={{
//         display: "flex",
//         justifyContent: "flex-start",
//         alignItems: "center",
//         transition: "opacity 0.2s ease 0s, visibility 0.3s ease 0s",
//         userSelect: "none",
//         width: "100%"
//       }}>
//       <img style={{height: "96px", paddingRight: "8px"}} src={Logo} />
//       <h1>grounds</h1></div>
//     </div>
//     <div style={{
//     maxWidth: "728px",
//     padding: "32px 0px",
//     zIndex: 1,
//     position: "relative",
//     width: "100%",
//     margin: "0px auto"
// }}>
//       {error
//         ? <p>{renderErrorMessage(error)}</p>
//         : <></>}
//       <RegionSelect setter={setRegion} context={context} />
//       <Input
//         type="text"
//         ref={summonerNameRef}
//         placeholder="Summoner Name"
//         required
//         />
//       <Btn type="submit">VALIDATE</Btn>
//         </div>
//       {/* <h4>Whilst playing the competitve <Link to="/about#games">games</Link> you love</h4> */}
//       <div style={{
//     display: "table",
//     margin: "0px auto",
// }}>
//         <a style={{
//     position: "relative",
//     width: "100%",
//     boxSizing: "border-box",
//     display: "flex",
//     alignItems: "flex-end",
//     WebkitBoxPack: "center",
//     justifyContent: "center",
//     cursor: "pointer",
//     outline: "none",
//     textDecoration: "none",
//     overflow: "hidden",
//     userSelect: "none",
//     padding: "15px 28px",
//     borderRadius:6,
//     background: "rgb(224, 63, 84)",
// }}>
//           <span style={{
//     whiteSpace: "nowrap",
//     color: "rgb(255, 255, 255)",    
//     fontSize:14,
//     fontWeight:500,
//     lineHeight:1.1,
//     paddingTop:2,
// }}>Download Free</span>
//           <div style={{
//     fontSize: "1rem",
//     color: "rgb(255, 255, 255)",
//     marginLeft:12
// }}>
//             <svg viewBox="0 0 32 32" style={{
//               float: "right",
//     width: "1em",
//     height: "1em",
//     strokeWidth:0,
//     stroke: "currentcolor",
//     fill: "currentcolor",
//     pointerEvents: "none",
// }}>
//   <title>windowsIcon</title>
//   <path d="M14 4.667v10.667h-11.333v-9.109l11.333-1.558zM2.667 25.758v-9.091h11.333v10.667l-11.333-1.576zM14.667 27.302v-10.635h14.667v12.667l-14.667-2.031zM14.667 4.679l14.667-2.012v12.667h-14.667v-10.654z"></path>
// </svg><svg viewBox="0 0 32 32" style={{
//     float: "right",
//     paddingRight: "px",
//     width: "1em",
//     height: "1em",
//     strokeWidth:0,
//     stroke: "currentcolor",
//     fill: "currentcolor",
//     pointerEvents: "none",
// }}>
//   <title>placeHolder</title>
//   <path d="M14 4.667v10.667h-11.333v-9.109l11.333-1.558zM2.667 25.758v-9.091h11.333v10.667l-11.333-1.576zM14.667 27.302v-10.635h14.667v12.667l-14.667-2.031zM14.667 4.679l14.667-2.012v12.667h-14.667v-10.654z"></path>
// </svg>
//           </div>
//         </a>
//       </div>
//     </form>
//     </div>
//     </div>
//     <div style={{width: "100%",
//     paddingBottom:44}}>
//       <div style={{    position: "relative",
//     maxWidth:1920,
//     width: "70%",
//     margin: "0px auto"}}>
//       <div style={{
//     display: "flex",
//     WebkitBoxPack: "justify",
//     justifyContent: "space-between",
//     WebkitBoxAlign: "center",
//     alignItems: "center",
// }}>
//        <h2 style={{fontWeight:700,
//     lineHeight:1.25, fontSize:"40px"}}>Competitve games you love, supported</h2>
//       </div>
//       </div>
//       <div style={{        position: "relative",
//         width: "87%",
//         overflow: "hidden",
//         margin: "0px auto",
//         paddingTop:20,
//       }}>
//         <div style={{
//               boxSizing: "border-box",
//               WebkitUserSelect: "none",
//               MozUserSelect: "none",
//               userSelect: "none",
//               WebkitTouchCallout: "none",
//               KhtmlUserSelect: "none",
//               touchAction: "pan-y",
//               WebkitTapHighlightColor: "transparent",
//               position: "relative",
//             display: "block",
//         }}>
//           <div style={{overflow: "visible",WebkitTransform: "translate3d(0,0,0)",
//     OTransform: "translate3d(0,0,0)",
//     transform: "translate3d(0,0,0)",     margin:0,
//     padding:0,    position: "relative",
//     display: "block",}}>
//             <div style={{
//               position: "relative",
//               top:0,
//               left:0,
//               minWidth:1300,
//               display: "flex",
//               WebkitBoxPack: "center",
//               justifyContent: "center",
//               width: "1300px !important",
//               opacity:1,
//               transform: "translate3d(0px, 0px, 0px)",
//             }}>
//               <div
//                 style={{
//                   outline: "none",
//                   display: "block",
//                   transition: "none 0s ease 0s !important",
//                   animation: "0s ease 0s 1 normal none running none !important",
//                   float: "left",
//                   height: "100%",
//                   minHeight:1,
//                   boxSizing: "border-box",
//                   WebkitUserSelect: "none",
//                   MozUserSelect: "none",
//                   userSelect: "none",
//                   WebkitTouchCallout: "none",
//                   KhtmlUserSelect: "none",
//                   touchAction: "pan-y",
//                   WebkitTapHighlightColor: "transparent",
//                 }}><div>
//                   </div>
//                     <div style={{
//                       position: "relative",
//                       width:200,
//                       marginRight:25,
//                       cursor: "pointer",
//                     }}>
//                       <div style={{
//                             position: "relative",
//                             display: "block",
//                             height:280,
//                             transition: "transform 0.3s ease 0s",
//                       }}>
//                         <img style={{
//                           userSelect: "none",
//                               display: "block",
//                               position: "relative",
//                               width: "100%",
//                               zIndex:1,
//                               opacity:1,
//                         }} src="https://theme.zdassets.com/theme_assets/43400/87a1ef48e43b8cf114017e3ad51b801951b20fcf.jpg" />
//                         {/* https://cdn1.epicgames.com/fn/offer/14BR_BPLaunch_EGS_S2_1200x1600-1200x1600-d31d390c22d23717659df2b807342d25.jpg?h=854&resize=1&w=640 */}
//                         {/* https://prod.cdn.earlygame.com/uploads/images/_square/dota-2-logo_200701_134115.png?mtime=20200701154116&focal=none&tmtime=20201014210104 */}
//                         {/* https://static.wikia.nocookie.net/cswikia/images/1/1e/Csgo_steam_store_header_latest.jpg/revision/latest?cb=20170630034202 */}
//                         <div style={{
//                               position: "absolute",
//                               top:20,
//                               left: "50%",
//                               width: "80%",
//                               height: "100%",
//                               visibility: "hidden",
//                               opacity:0,
//                               backgroundImage: "url(/static/media/Lol.c7035955.png)",
//                               backgroundPosition: "center center",
//                               backgroundRepeat: "no-repeat",
//                               backgroundSize: "cover",
//                               filter: "blur(20px)",
//                               transform: "translateX(-50%)",
//                               transition: "opacity 0.3s ease 0s, visibility 0.3s ease 0s",
//                         }}></div>
//                       </div>
//                       <div style={{    display: "flex",
//     flexDirection: "column",
//     textAlign: "center",
//     paddingTop:12,}}></div>
//                     </div>
//                   </div>
//               <div></div>
//               <div></div>
//               <div></div>
//               <div></div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//     <div>
//         {/* <h3>Build a <Link to="/about#persona">persona</Link></h3>
//         <h3>Complete <Link to="/about#challenges">challenges</Link></h3>
//         <h3>Earn <Link to="/about#crypto">crypto</Link></h3> */}

//     </div>
//   </div>
// };


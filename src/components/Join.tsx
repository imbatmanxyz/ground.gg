import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Container, Button, Grid } from '@material-ui/core';
import { Env } from '../context';
import { Navbar } from './Navbar';
import styled from 'styled-components';


enum JoinStatus {
  success, fail
};

const Btn = styled(Button)({
  background: 'linear-gradient(45deg, #16BA97 10%, #5AC7AF 90%)',
  color: '#FFFFF2',
  fontWeight: "bold",
  fontSize: "16px",
  marginTop: "16px",
  padding: "16px",
  borderRadius: "0.5em",
});

const Input = styled.input`
  background: #FFFFF2;
  border-radius: 0.5em;
  border: none;
  padding: 0.8em;
  margin-top: 8px;
    
  color: #40403D;
  font-size: 1.1em;
  padding-left: 1.5em;
    
  outline: none;
  &::placeholder {
    font-family: Signika;
    font-weight: 400;
  }
`;

export function Join({ env }: { env: Env; }) {
  const { profileId } = useParams();
  const navigate = useNavigate();
  const [joinRes, setJoinRes] = React.useState<JoinStatus | null>(null);
  const [errors, setErrors] = React.useState<string>();
  const emailAddress = React.useRef<HTMLInputElement>(null);

  React.useEffect(() => {
    if (joinRes === JoinStatus.success)
      navigate(`/claim-account/${profileId}`, { replace: true });
    else
      setErrors("");
  }, [joinRes]);

  async function handleSubmit(evt: React.FormEvent<HTMLFormElement>) {
    evt.preventDefault();
    const req = await fetch(`${env.urlBase}join`, {
      method: "POST",
      headers: {
        'Content-Type': "application/json"
      },
      body: JSON.stringify({
        email: emailAddress.current?.value,
        profileId: profileId
      })
    });

    if (req.status === 204)
      setJoinRes(JoinStatus.success);
    else
      setJoinRes(JoinStatus.fail);
  }

  return <>
      <Navbar />
    <Container maxWidth="sm">
  <form onSubmit={handleSubmit}>
    <h1>JOIN</h1>
    <Grid container direction="column" justify="flex-start" alignItems="stretch">
      <Input type="email" ref={emailAddress} placeholder="email address" />
      <Btn type="submit">JOIN</Btn>
    </Grid>
    {errors ? <h1>Please try again or contact <a href="mailto:grounds.gg@gmail.com">grounds.gg@gmail.com</a></h1> : <></>}
  </form>
  </Container>
  </>
}

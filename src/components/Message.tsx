import styled from 'styled-components';

export const Message = styled.article`
  grid-area: message;
  grid-column: 1/13;
  margin: 0 auto;
`;

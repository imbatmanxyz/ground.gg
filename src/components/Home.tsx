import React from 'react';
import styled from 'styled-components';
import { EnvContext } from '../context';
import { Header } from './Header';
import { Navbar } from './Navbar';
import { GameList } from './GameList';
import { Title } from '../themes/typography';
import { Persona } from './Persona';
import { Challenge } from './ChallengeList';
import { Reward } from './Reward';


export function Home() {
  const Container = styled.div`
    display: grid;
    grid-template-areas:
        "header"
        "content"
        "message"
        "persona"
        "challenges"
        "crypto";
    grid-template-columns: repeat(12, 1fr);
    grid-gap: 20px;
  `

  const Message = styled.article`
    grid-area: message;
    grid-column: 1/13;
    margin: 0 auto;
  `;

  return <Container>
    <Navbar />
    <EnvContext.Consumer>
      {
        context => (
          <>
            <Header context={context} />
            <GameList context={context} />
          </>
        )
      }
    </EnvContext.Consumer>
    <Message>
      <Title>Gamify your way to victory and rewards</Title>
    </Message>
    <Persona />
    <EnvContext.Consumer>
      {
        context => (
          <Challenge context={context} />
        )
      }
    </EnvContext.Consumer>
    <Reward />
  </Container>
}
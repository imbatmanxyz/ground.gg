import React from 'react';
import styled from 'styled-components';
import Images from '../assests/index';
import { SubTitle } from '../themes/typography';


interface Logo {
    small?: boolean,
    medium?: boolean,
    large?: boolean
}

export function Logo(props: Logo) {
  const Container = styled.div`
    display: flex;
    align-items: center;
    user-select: none;
    ${props.large && `margin-bottom: 48px;`}
    > img {
      height: ${props.small ? `48px;`
      : props.medium ? `64px;`
        : props.large ? `128px;` : `32px;`}
    }
    > h2 {
      margin-left: 8px;
      ${props.large && `font-size: 48px;`}
    } 
  `;

  return <Container>
    <img src={Images.logo} />
    <SubTitle>{props.large ? "rounds" : "rounds.gg"}</SubTitle>
  </Container>;
}

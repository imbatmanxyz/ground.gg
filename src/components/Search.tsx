import React from 'react';
import styled from 'styled-components';

export const Search = () => {
  const [showDropDownContent, setShowDropDownContent] = React.useState(false);
  const [region, setRegion] = React.useState(["EUW", "NA", "EUNE", "KR"][0]);

  const SearchWrapper = styled.div`
    border-radius: 8px;
    background-color: #F0F0E4;
    display: flex;
    align-items: center;
    width: 80%;
  `;
  const DropDown = styled.div`
    padding: 24px;
    user-select: none;
    &>span {
      color: #232323;
      font-weight: bold;
      font-size: 16px;
    }
    &:hover {
      cursor: pointer;
      border-radius: 8px 0px 0px 8px;
      background-color: rgb(235, 235, 235);
    }
    &> .title {
      margin-right: 8px;
    }
    &> .region {
      padding: 8px;
      border-radius: 8px;
      background-color:  rgb(204, 204, 204);
    }
    &> svg {
      width: 16px;
      height: 16px;
    }
  `;
  const DropDownContent = styled.div<{ showDropDownContent: boolean; }> `
    ${props => props.showDropDownContent ? `` : `display: none;`}
    position: absolute;
    height: fit-content;
    border-radius: 8px;
    background-color: #F0F0E4;
    width: 168px;
    margin-top: 224px;
    &> div {
      padding: 8px;
      font-weight: bold;
    }
    &> div:hover {
      background-color: #F44B44;
      /* margin: 8px; */
      border-radius: 8px;
      font-weight: bold;
      color: #FFFFF2
    }
  `;
  const Input = styled.div`
    padding: 24px;
    &> input {
      min-width: auto;
      width: 368px;
      border: none;
      font-size: 16px;
      font-family: 'Montserrat';
      background-color: #F0F0E4;
    }
    &> input:focus {
      outline: none;
      border: none;
    }
    &> input::placeholder {
      font-size: 16px;
      font-family: 'Montserrat';
    }
  `;
  const Icon = styled.div`
    &> svg {
      height: 32px;
      width: 32px;
      cursor: pointer;
    }
  `;

  return <SearchWrapper>
    <DropDown onClick={() => setShowDropDownContent(!showDropDownContent)}>
      <span className="title">Regions</span>
      <span className="region">{region}</span>
      <svg viewBox="0 0 32 32"><title>arrow-dropdown</title><path d="M8 12l8 8 8-8z"></path></svg>
    </DropDown>
    <DropDownContent showDropDownContent={showDropDownContent}>
      {["EUW", "NA", "EUNE", "KR"].map((value: string, index: number) => {
        return <div key={index} onClick={() => {
          setRegion(value);
          setShowDropDownContent(!showDropDownContent);
        }}>{value}</div>;
      })}
    </DropDownContent>
    <Input>
      <input className="input" type="text" placeholder="enter username" />
    </Input>
    <Icon onClick={() => console.log("dsd")}>
      <svg viewBox="0 0 20 20"><title>search</title><path fill-rule="evenodd" clip-rule="evenodd" d="M14 9.5C14 11.9853 11.9853 14 9.5 14C7.01472 14 5 11.9853 5 9.5C5 7.01472 7.01472 5 9.5 5C11.9853 5 14 7.01472 14 9.5ZM14.7489 13.3347C15.5356 12.2597 16 10.9341 16 9.5C16 5.91015 13.0899 3 9.5 3C5.91015 3 3 5.91015 3 9.5C3 13.0899 5.91015 16 9.5 16C10.9341 16 12.2597 15.5356 13.3347 14.7489L15.2928 16.707C15.6833 17.0976 16.3165 17.0976 16.707 16.707C17.0976 16.3165 17.0976 15.6833 16.707 15.2928L14.7489 13.3347Z"></path></svg>
    </Icon>
  </SearchWrapper>;
};

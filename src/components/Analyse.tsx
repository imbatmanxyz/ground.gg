import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Env } from '../context';
import { database } from '../firebaseConfig';
import { Profile } from './Profile';
import { Navbar } from './Navbar';

import { Container, Grid, Button, withStyles, createStyles, Theme } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import styled from 'styled-components';

import FreshBlood from '../assests/freshBlood.svg'
import HotStreak from '../assests/hotStreak.svg'
import InActive from '../assests/inActive.svg'


export function canConnect(profileId: string): boolean {
  return profileId ? true : false;
}
  
export interface Profile {
  currentVersion: string,
  freshBlood: boolean,
  hotStreak: boolean,
  inActive: boolean,
  leaguePoints: number,
  losses: number,
  wins: number,
  veteran: boolean,
  tier: string,
  rank: string,
  emblemUrl: string,
  summonerName: string,
  summonerLevel: number,
  mostPlayedChampionUrl: string,
  profileIconId: number,
  trophiesEarned: string[]
}

export function profileIcon(version: string | undefined, profileIcondId: number | undefined) {
    return `http://ddragon.leagueoflegends.com/cdn/${version}/img/profileicon/${profileIcondId}.png`
  }
  
export function tierEmblem(tierEmblem: string | undefined, urlBase: string) {
  return `${urlBase}ranked-assets/emblems/${tierEmblem}.png`;
}


interface TabPanelProps {
  children?: React.ReactNode;
  value: any;
  index: any;
  className: string;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

interface StyledTabProps {
  label: string;
}

interface StyledTabsProps {
  value: number;
  onChange: (event: React.ChangeEvent<{}>, newValue: number) => void;
}

const StyledTabs = withStyles({
  root: {
    backgroundColor: '#000016',
    '& > div > svg': {fill: '#FFFFF2'}
  },
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    '& > span': {
      maxWidth: 40,
      width: '100%',
      backgroundColor: '#788CFF',
    },
  },
})((props: StyledTabsProps) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const StyledTab = withStyles((theme: Theme) =>
  createStyles({
    root: {
      textTransform: 'none',
      color: '#FFFFF2',
      fontWeight: theme.typography.fontWeightRegular,
      fontSize: theme.typography.pxToRem(15),
      marginRight: theme.spacing(1),
      '&:focus': {
        opacity: 1,
      },
    },
    selected: {
      color: '#788CFF',
      fontWeight: 'bold'
    }
  }),
)((props: StyledTabProps) => <Tab disableRipple {...props} />);

function a11yProps(index: any) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const RoundedIcon = styled.img`
  border-radius: 50%;
  height: ${props => props.height};

`

const Btn = styled(Button)({
  background: 'linear-gradient(45deg, #16BA97 10%, #5AC7AF 90%)',
  color: '#FFFFF2',
  fontWeight: "bold",
  fontSize: "16px",
  marginTop: "16px",
  padding: "16px",
  borderRadius: "0.5em",
});

function numeric(rank: string | undefined): number | null {
  const conversion: {[key: string]: number} = {
    "I": 1,
    "II": 2,
    "III": 3,
    "IV": 4
  }

  return rank === undefined ? null : conversion[rank]
}

const ChampionImg = styled.img`
  border-radius: 24px;
  width: 475px;
  padding-top: 16px;
  @media (max-width: 500px) {
    width: 275px;
    padding-top: 16px;
  }
`

const ViewMore = styled.a`
  color: #788CFF;
  text-decoration: none;
  &:hover {
    color: #FFFFF2;
  }
`

export function Analyse({ env }: { env: Env; }) {
  const [value, setValue] = React.useState(0);
  const [profileInfo, setProfileInfo] = React.useState<Profile>();

  const { profileId } = useParams();
  const navigate = useNavigate();

  React.useEffect(() => {
    if (!canConnect(profileId))
      navigate("/", { replace: true });

    database.ref(profileId).on("value", (snapshot) => {
      setProfileInfo(snapshot.val() as Profile);
    });
  }, []);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (<div>
      <Navbar />
    <Container maxWidth="sm">
      <AppBar position="static" color="default">
        <StyledTabs
          value={value}
          onChange={handleChange}
          aria-label="scrollable auto tabs example"
        >
          <StyledTab label="Profile" {...a11yProps(0)} />
          <StyledTab label="Trophies" {...a11yProps(1)} />
          <StyledTab label="Challenges" {...a11yProps(2)} />
        </StyledTabs>
      </AppBar>
      <TabPanel className="profile" value={value} index={0}>
      <Grid container>
        <h1>Crypto pot: 245 gg</h1>
        <Grid container item style={{backgroundColor: "#14182B", borderRadius: "24px", padding: "0px 8px 0px 8px"}}>
          <RoundedIcon src={profileIcon(profileInfo?.currentVersion, profileInfo?.profileIconId)} height="128px" alt="Profile Icon" />
          <Grid item>
            <h2>L {profileInfo?.summonerLevel}</h2>
            <h2>{profileInfo?.summonerName}</h2>
          </Grid>
        </Grid>
        <h1>Solo Queue</h1>
        <Grid container item style={{backgroundColor: "#14182B", borderRadius: "24px", padding: "0px 8x 0px 8px"}}>
          <RoundedIcon src={tierEmblem(profileInfo?.emblemUrl, env.domain)} height="128px" alt="Tier Emblem" />
          <Grid item>
            <h2>{profileInfo?.tier} {numeric(profileInfo?.rank)}</h2>
            <h2>W {profileInfo?.wins}</h2>
          </Grid>
          <Grid item>
            <h2 style={{paddingLeft: "16px"}}>{profileInfo?.leaguePoints} LP</h2>
            <h2 style={{paddingLeft: "16px"}}>L {profileInfo?.losses}</h2>
          </Grid>
        </Grid>
        <h1>Activity</h1>
        <Grid 
          container item justify="space-evenly"
          alignItems="center" style={{backgroundColor: "#14182B", borderRadius: "24px", padding: "16px 8px 0px 8px"}}>
          {profileInfo?.freshBlood ? <img src={FreshBlood} style={{height: "72px"}} /> :  <img src={FreshBlood} style={{opacity: 0.3, height: "72px"}} />}
          {profileInfo?.inActive ? <img src={InActive} /> :  <img src={InActive}  style={{opacity: 0.3, height: "64px"}} />}
          {profileInfo?.hotStreak ? <img src={HotStreak} /> :  <img src={HotStreak} style={{opacity: 0.3, height: "72px"}} />}
          <Grid container item alignItems="center" justify="center">
           <ChampionImg src={profileInfo?.mostPlayedChampionUrl} alt="Champion Image" />
          </Grid>
        </Grid>
      </Grid>
      </TabPanel>
      <TabPanel className="trophy" value={value} index={1}>
        {profileInfo?.trophiesEarned?.map((value: string, index: number) => {
        return <h2 key={index}>{value}</h2>;
        })}
        <h2>{profileInfo?.veteran}</h2>
        <ViewMore href={`/join/${profileId}`}>View your trophies</ViewMore>
      </TabPanel>
      <TabPanel className="challenge" value={value} index={2}>
        <h2>Nosferatu</h2>
        <h2>Napoleon Dynamite</h2>
        <h2>Leeroy Jenkins</h2>
        <h2>More Rick than Morty</h2>
        <ViewMore href={`/join/${profileId}`}>View your challenges</ViewMore>
      </TabPanel>
      <Grid container item direction="column" alignContent="stretch">
        <Btn onClick={() => navigate(`/join/${profileId}`, { replace: true })}>Analyse Account</Btn>
      </Grid>
      </Container></div>
  )
}

import React from 'react';
import styled from 'styled-components';
import Images from '../assests/index';
import { Alert } from './Alert';
import { SubTitle } from '../themes/typography';
import { Env } from '../context';


const Games = () => {
  const Wrapper = styled.div`
    display: flex;
    align-items: center;
    overflow-y: hidden;
    &::-webkit-scrollbar {
      border-radius: 8px;
    }
    &::-webkit-scrollbar-track {
      background: #F44B44; 
    }
    &::-webkit-scrollbar-thumb {
      background: #FFFFF2;
    }
    &::-webkit-scrollbar-thumb:hover {
      background: #FFFFF2; 
    }
  `;

  const Item = styled.div<{ active: boolean; }> `
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    width: 256px;
    height: 312px;
    &> img {
      height: 256px;
      width: 196px;
      margin: 0 8px 0 8px;
      filter: ${props => props.active ? `none` : `grayscale(100%)`};
    }
    &> img:hover {
      filter: none;
      cursor: pointer;
    }
  `;

  return <Wrapper>
    {[
      { "title": "League of Legends", "img": Images.Lol, "active": true },
      { "title": "Dota 2", "img": Images.Dota, "active": false },
      { "title": "CS:GO", "img": Images.CsGo, "active": false },
      { "title": "Fortnite", "img": Images.Fortnite, "active": false },
      { "title": "Overwatch", "img": Images.Overwatch, "active": false },
      { "title": "Hearthstone", "img": Images.Hearthstone, "active": false }
    ].map(({ title, img, active }: { title: string; img: string; active: boolean; }, index: number) => {
      return <Item active={active} onClick={() => Alert(title)}>
        <img src={img} />
        <h3>{title}</h3>
      </Item>;
    })}
  </Wrapper>;
};


export function GameList({ context }: { context: Env }) {
  const Container = styled.article`
    grid-area: content;
    grid-column: 3 / 11;
    margin-top: 64px;
  `

  const Head = styled.div`
    display: flex;
    align-items: center;
    &> span {
      margin-left: 16px;
      font-style: italic;
      color: #b8b8b8;
    }
  `

  return <Container>
    <Head>
      <SubTitle>Supported Games</SubTitle>
      <span>and coming soon</span>
    </Head>
    <Games />
  </Container>
}
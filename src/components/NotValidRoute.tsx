import React from 'react';
import { Link } from 'react-router-dom';

export function NotValidRoute() {
  return <h1>404 please navigate <Link to="/">back</Link></h1>;
}

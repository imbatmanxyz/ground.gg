import React from "react";
import styled from 'styled-components';
import { Logo } from './Logo';


export function Navbar() {
  const Container = styled.nav`
    position: fixed;
  `;

  return <Container>
    <Logo small />
  </Container>
}
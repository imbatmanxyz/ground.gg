import React from 'react';
import { Navbar } from './Navbar';
import { Container, Grid } from '@material-ui/core';


export function ClaimAccount() {
  return <><Navbar /><Container maxWidth="sm">
    <h1>Claim Account</h1>
    <p>Thank you for joining</p>
    <p>The full platform will be available soon</p>
    <p>You'll be able to claim your account later</p>
    <h2>Coming soon</h2>
    </Container>
  </>;
}

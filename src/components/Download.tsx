import React from 'react';
import styled from 'styled-components';
import { Alert } from './Alert';

export const Download = () => {
  const Btn = styled.button`
    display: flex;
    align-items: center;
    padding: 8px 24px 8px 24px;
    border-radius: 8px;
    outline: none;
    border: none;
    margin-top: 24px;
    background-color: #F44B44;
    color: #FFFFF2;
    font-weight: bold;
    font-size: 16px;
    &:hover {
      cursor: pointer;
    }
  `;
  const SvgWrapper = styled.span`
    height: 24px;
    width: 24px;
    margin-right: 8px;
  `;

  return <Btn onClick={() => Alert("Desktop App")}>
    <SvgWrapper>
      <svg viewBox="0 0 32 32"><title>windowsIcon</title><path d="M14 4.667v10.667h-11.333v-9.109l11.333-1.558zM2.667 25.758v-9.091h11.333v10.667l-11.333-1.576zM14.667 27.302v-10.635h14.667v12.667l-14.667-2.031zM14.667 4.679l14.667-2.012v12.667h-14.667v-10.654z" fill="#FFFFF2"></path></svg>
    </SvgWrapper>Download free
  </Btn>;
};

import React from 'react';
import { useNavigate } from 'react-router-dom';

export function Login() {
  const navigate = useNavigate();

  return <><h1>Login</h1><button onClick={() => navigate('/', { replace: true })}>Analyse Profile</button></>;
}

import React from 'react';
import { Env } from '../context';
import styled from 'styled-components';

const DropDown = styled.select`
  // background: #FFFFF2;
  // border-radius: 0.5em;
  // border: none;
  // padding: 0.8em;
  // margin-top: 15px;
    
  // color: #40403D;
  // font-size: 1.1em;
  // padding-left: 1.5em;
  // font-family: Signika;
  // font-weight: 400;
    
  // outline: none;
`

const MenuItem = styled.option``

export function RegionSelect({ setter, context }: { setter: (region: string) => void; context: Env; }) {
  const [regions, setRegions] = React.useState<{
    [key: string]: string;
  }>();

  React.useEffect(() => {
    fetch(`${context.domain}regions.json`)
      .then(res => res.json())
      .then(json => setRegions(json));
  }, []);

  return <>
    {!!regions
      ? <DropDown
        defaultValue={regions["euw"]}
        onChange={e => setter(e.target.value)}
        required
      >
        {Object.keys(regions).map((value: string, index: number) => {
          return <MenuItem key={index} value={regions[value]}>{value.toUpperCase()}</MenuItem>;
        })}
      </DropDown>
      : ""}
  </>;
}

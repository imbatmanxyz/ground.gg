import React from 'react';
import { Navigate, useRoutes } from 'react-router-dom';
import { Env, Auth } from '../context';
import { Home } from './Home';
import { Join } from './Join';
import { InValid } from './InValid';
import { Claim } from './Claim';
import { Persona } from './Persona'
// import { About } from './About';
// import { Login } from './Login';
// import { Profile } from './Profile';
// import { Analyse } from './Analyse';


export const Router = ({ auth, env }: { auth: Auth; env: Env; }) => {
  return useRoutes([
    {
      path: '/',
      element: auth.isLoggedIn ? <Navigate to={`/profile/${auth.userId}`} /> : <Home />
    },
    // {
    //   path: 'profile',
    //   element: !auth.isLoggedIn ? <Navigate to='/login' /> : "",
    //   children: [
    //     { path: '/', element: <NotValidRoute /> },
    //     { path: ':profileId', element: <Profile /> }
    //   ]
    // },
    // { path: '/about', element: <About /> },
    // { path: '/login', element: <Login /> },
    {
      path: 'join',
      element: auth.isLoggedIn ? <Navigate to={`/profile/${auth.userId}`} /> : "",
      children: [
        { path: '/', element: <InValid /> },
        { path: ':profileId', element: <Join env={env} /> }
      ]
    },
    {
      path: 'claim-account',
      children: [
        { path: '/', element: <InValid /> },
        { path: ':profileId', element: <Claim /> }
      ]
    },
    {
      path: 'persona',
      element: "",
      children: [
        { path: '/', element: <InValid /> },
        { path: ':profileId', element: <Persona env={env} /> }
      ]
    }
  ]);
};

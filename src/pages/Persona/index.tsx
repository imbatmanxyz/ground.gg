import React, { useState } from "react";
import styled from "styled-components";
import { Env } from "../../context";
import Seraphine from "../../assests/SERAPHINE_SOCIAL_ICON_FINAL.jpg";
import { Thin, Title } from "../../themes/typography";
import DownArrow from "../../assests/downArrow.svg";
import { useParams } from 'react-router-dom';


const Container = styled.div`
    display: grid;
    grid-template-areas:
        "rank"
        "reputation"
        "wallet"
        "challenge"
        "history";
    grid-template-columns: 1fr 2fr 1fr;
    grid-template-rows: 1fr 1fr;
    grid-gap: 16px;
`

const Area = styled.div<{area: string, col?: number, row?: number}>`
    grid-area: ${props => props.area};
    background-color: #FFFFF2;
    grid-column: ${props => props.col};
    grid-row: ${props => props.row};
`

const Row = styled.header`
    display: flex;
    flex-direction: row;
`

const Col = styled.aside`
    display: flex;
    flex-direction: column;
`

const Btn = styled.button<{color?: string}>`
    display: flex;
    flex-direction: row;
    color: ${props => props.color ?? "#FFFFFF"};
    background-color: #F0F0E4;
    padding: 2px 16px;
    align-items: center;
    width: 70%;
    justify-content: space-between;
    border-radius: 8px;
    border: none;
    &:focus {
        outline: none;
    }
`

interface ImgAttr {
    color?: string,
    border?: string,
    height?: string,
    backgroundColor?: string
}

const Icon = styled.img<ImgAttr>`
    height: 128px;
    border-radius: 50%;
    border: ${attrs => `${attrs.border} ${attrs.color}`};
    margin: 16px;
    background-color: ${attrs => attrs.backgroundColor ?? `none`};
`

const Badge = ({attr, children}: {attr: string, children: string}) => {
    const Container = styled.div`
        background-color: ${attr};
        padding: 8px;
        width: fit-content;
        border-radius: 8px;
    `
    return <Container>{children}</Container>
}

const Profile = () => {
    const [select, setSelect] = useState<string>("")

    const DropDown = () => {
        const [hide, setHide] = useState<boolean>(true)

        interface Content {
            display: React.Dispatch<React.SetStateAction<boolean>>,
            hidden: boolean,
            select: React.Dispatch<React.SetStateAction<string>>
        }

        const Content = ({display, hidden, select}: Content) => {
            if (hidden) return <></>

            const Container = styled.div`
                position: absolute;
                margin-top: 64px;
                background-color: #F0F0E4;
                border-radius: 8px; 
            `
            const Item = styled.div`
                padding: 4px 16px;
                &:hover {
                    background-color: #c4c4c4;
                    cursor: pointer;
                    border-radius: 8px;
                }
            `

            function selectItem(evt: React.MouseEvent<HTMLDivElement, MouseEvent>) {
                evt.preventDefault();
                display(!hidden)
                select(evt.currentTarget.innerText)
            }

            return <Container>
                {
                    ["SOLO_QUEUE", "FLEX_QUEUE"].map((value: string, index: number) => {
                        return <Item
                            onClick={selectItem}
                            key={index}>{value}</Item>
                    })
                }
            </Container>
        }

        const Container = styled.div`
            display: flex;
        `

        return <Container>
            <Btn onClick={() => setHide(!hide)} color="#232323">
                <Title fontSize="32px" fontWeight="900">ranks</Title>
                <img src={DownArrow} style={{width: "24px"}} />
            </Btn>
            <Content hidden={hide} display={setHide} select={setSelect} />
        </Container>
    }

    return <Area area="rank" col={1} row={1}>
        <Row>
            <Icon src={Seraphine} color="#EAD2F0" border="8px solid" />
            <Col>
                <Title
                    fontSize="32px"
                    margin="8px 0px 0px 0px">Speedy McJingles</Title>
                <Thin>last played 3 hours</Thin>
                <DropDown />
            </Col>
        </Row>
        <Row>
            <Title>{select}</Title>
        </Row>
    </Area>
}

const Reputation = () => {
    return <Area area="reputation" col={2} row={1}>
        <h1>REPUTATION</h1> 
    </Area>
}

const Wallet = () => {
    return <Area area="wallet" col={3} row={1}>
        <h1>WALLET</h1> 
    </Area>
}

const Challenges = () => {
    return <Area area="challenge" col={1} row={2}>
        <h1>CHALLENGES</h1> 
    </Area>
}

const History = () => {
    return <Area area="history" col={2} row={2}>
        <h1>MATCH HISTORY</h1> 
    </Area>
}

export function Persona({ env }: { env: Env }) {
    const { profileId } = useParams();
    // const {state, data} = useQueue<string>(profileId)

    return <Container>
        <Profile />
        <Reputation />
        <Wallet />
        <Challenges />
        <History />
    </Container>
}
import React from 'react';


export interface Env {
    domain: string,
    urlBase: string
};

export interface Auth {
    isLoggedIn: boolean,
    userId: string | null
};

export const EnvContext = React.createContext<Env>({
    domain: "",
    urlBase: ""
});

export const AuthContext = React.createContext<Auth>({
    isLoggedIn: false,
    userId: null
});
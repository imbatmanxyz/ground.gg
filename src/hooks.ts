// export function useTest<T>(Component: () => JSX.Element) {
//     return () => {
//         return <Component />
//     }
// }

import React from "react";
import Axios from "axios";
import {database} from "./firebaseConfig";


type States = "idle" | "loading" | "success" | "error";

interface Message<T> {
    state: States,
    data: T | null
}

interface Dispatch<T> {
    type: States,
    payload: T | null
}

export function useQueue<T>(profileId: string): Message<T> {
    const result: Message<T> = {
        state: "idle",
        data: null
    }

    const [state, dispatch] = React.useReducer((state: Message<T>, action: Dispatch<T>) => {
		switch (action.type) {
			case 'loading':
				return { ...result, status: 'loading' };
			case 'success':
				return { ...result, status: 'success', data: action.payload };
			case 'error':
				return { ...result, status: 'error', error: action.payload };
			default:
				return state;
		}
	}, result);

    React.useEffect(() => {
        if (!profileId) return

        database.ref(profileId).on("value", (snapshot) => {
            const payload = snapshot.val() as Message<T>

            dispatch({
                type: payload.state,
                payload: payload.data
            });
        });

        return () => {
            database.ref(profileId).off()
        }
    }, [profileId])

    return state
}

interface Response<T> {
    state: States,
    data: T
}

async function useAxios<T>(url: string): Promise<Response<T>> {
    const request = await Axios.request<T>({
        url: url,
        transformResponse: (r: T) => r
    })
    const {data} = request

    return {
        state: "idle",
        data: data
    }
}
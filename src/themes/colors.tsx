import { defaultCipherList } from "constants";

export const colors = {
    "orange": "#DE6449",
    "purple": "#A593E0",
    "buttonL": "#C23B36",
    "bottonR": "#F54B45",
    "black": "#40403D",
    "jet": "#323232",
    "navy": "#020736",
    "white": "#FFFFF2",
    "green": "#00D969",
    "offWhite": "#F0F0E4"
}
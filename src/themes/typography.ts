import styled from 'styled-components';

interface Title {
  fontSize?: string,
  fontWeight?: string,
  margin?: string
}

export const Title = styled.h1<Title>`
  font-size: ${attrs => attrs.fontSize ?? "48px"};
  margin: ${attrs => attrs.margin ?? 0};
  color: #232323;
  font-weight: ${attrs => attrs.fontWeight ?? 700};
`

export const SubTitle = styled.h2`
  font-size: 24px;
`

export const Sub = styled.h2`
    font-size: 24px;
    font-weight: 600;
`

export const Thin = styled.h3`
  font-size: 12px;
  font-weight: 300;
  margin: 0px 0px 16px 0px;
`
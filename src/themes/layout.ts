import styled from 'styled-components';


export const Grid = styled.div`
  display: grid;
  grid-template-areas:
      "header"
      "content"
      "message"
      "persona"
      "challenges"
      "crypto";
  grid-template-columns: repeat(12, 1fr);
  grid-gap: 20px;
`
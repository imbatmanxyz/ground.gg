import React from 'react';
import { Router } from './pages/Router';
import { EnvContext, AuthContext } from './context';


const App = () => {
  return (<AuthContext.Consumer>
    {
      auth => (
        <EnvContext.Consumer>
          {
            env => (
              <Router auth={auth} env={env} />
            )
          }
        </EnvContext.Consumer>
      )
    }
    </AuthContext.Consumer>);
}

export default App;

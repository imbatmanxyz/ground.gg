import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./index.css";
import reportWebVitals from './reportWebVitals';
import { EnvContext, AuthContext } from './context';
import { BrowserRouter as Router } from 'react-router-dom';
import 'fontsource-roboto';
import "fontsource-varela"


function AuthProvider({children}: {children: any}) {
  return <AuthContext.Provider value={{isLoggedIn: false, userId: null}}>
    {children}
  </AuthContext.Provider>
} 

function EnvProvider({children}: {children: any}) {
  return <EnvContext.Provider value={
    process.env.NODE_ENV === 'production' ? {
      domain: "https://grounds.gg/",
      urlBase: "https://europe-west2-grounds-gg.cloudfunctions.net/"
    } : {
      domain: "http://localhost:3000/",
      urlBase: "https://europe-west2-grounds-gg.cloudfunctions.net/"
    }
  }>
    {children}
  </EnvContext.Provider>
}

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <EnvProvider>
        <AuthProvider>
          <App />
        </AuthProvider>
      </EnvProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

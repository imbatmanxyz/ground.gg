import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/analytics";
import "firebase/performance";

const firebaseConfig = {
    apiKey: "AIzaSyC0hN15-9TbFObQBKph-vEebouYh2tsUKY",
    authDomain: "grounds-gg.firebaseapp.com",
    databaseURL: "https://grounds-gg-4001d.europe-west1.firebasedatabase.app/",
    projectId: "grounds-gg",
    storageBucket: "grounds-gg.appspot.com",
    messagingSenderId: "1038470620308",
    appId: "1:1038470620308:web:2f9a552f7a56664257c081",
    measurementId: "G-2WVRN634H3"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const analytics = firebase.analytics();
export const metrics = firebase.performance();
export const database = firebase.database();
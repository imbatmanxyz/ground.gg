import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import cors from 'cors';
import axios from 'axios';
import {Basic, GetSummonerByName, MatchMetaData, ChampionMastery, Error, LeagueEntry} from './interfaces';
import crypto from 'crypto';

// import nodemailer from 'nodemailer';


const ENV = functions.config();
admin.initializeApp({
    databaseURL: ENV.db.realtime_uri
});
const FIRESTORE = admin.firestore();
const QUEUE = admin.database();

const corsMiddleWare = cors({ origin: true });
const httpsFunctions = functions.region("europe-west2").https;
const triggerFunctions = functions.region("europe-west2").firestore;

enum COLLECTIONS {
    profile="profile",
    mastery="mastery",
    league="league",
    match="match",
    champion="champion"
};

const RIOT_HEADER = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36",
    "Accept-Language": "en-ZA,en-GB;q=0.9,en-US;q=0.8,en;q=0.7",
    "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
    "Origin": "",
    "X-Riot-Token": ENV.riot.key
};

// let CURRENT_MAPS = [];
// let CURRENT_QUEUES = [];
let CURRENT_VERSION = "10.23.1";
// let CURRENT_CHAMPIONS = [];

// const MAPS = "http://static.developer.riotgames.com/docs/lol/maps.json";
// const QUEUE_URL = "http://static.developer.riotgames.com/docs/lol/queues.json";
// const VERSION_URL = "https://ddragon.leagueoflegends.com/api/versions.json";
const CHAMPION_DATA_URL = `http://ddragon.leagueoflegends.com/cdn/${CURRENT_VERSION}/data/en_US/champion.json`;


// const transporter = nodemailer.createTransport({
//     service: 'gmail',
//     auth: {
//         user: ENV.gmail.address,
//         pass: ENV.gmail.password
//     }
// });

function encrypt(region: string, summonerName: string) {
    const {algo, password, salt, buffer, update, final}: {
        algo: string,
        password: string,
        salt: string,
        buffer: string,
        update: any,
        final: string
    } = ENV.secure;

    const saltConfig = salt.split(" ");
    const key = crypto.scryptSync(password, saltConfig[0], +saltConfig[1]);
    const bufferConfig = buffer.split(" ");
    const iv = Buffer.alloc(+bufferConfig[0], +bufferConfig[1]);
    const cipher = crypto.createCipheriv(algo, key, iv);
    const updateConfig = update.split(" ");
    let encrypted = cipher.update(`${region}|+|${summonerName}`, updateConfig[0], updateConfig[1]);
    encrypted += cipher.final(final);
    return encrypted;
}

function deencrypt(profileId: string) {
    const {algo, password, salt, buffer, update, final}: {
        algo: string,
        password: string,
        salt: string,
        buffer: string,
        update: any,
        final: string
    } = ENV.secure;

    const saltConfig = salt.split(" ");
    const key = crypto.scryptSync(password, saltConfig[0], +saltConfig[1]);
    const bufferConfig = buffer.split(" ");
    const iv = Buffer.alloc(+bufferConfig[0], +bufferConfig[1]);
    const decipher = crypto.createDecipheriv(algo, key, iv);
    const updateConfig = update.split(" ");
    let decrypted = decipher.update(profileId, updateConfig[0], updateConfig[1]);
    decrypted  += decipher.final(final);
    return decrypted;
}

interface Join {
    email: string,
    profileId: string
}

export const join = httpsFunctions.onRequest(async (req, res) => {
    return corsMiddleWare(req, res, async() => {
        functions.logger.info("Request to join grounds.gg");
        const body: Join = req.body;
        let responseCode = 204;
        
        try {
            functions.logger.info("Updating profile with email address");
            await FIRESTORE.collection(COLLECTIONS.profile).doc(body.profileId).update({
                emailAddress: body.email
            });
            functions.logger.info("successfully updated profile with email address");
        } catch (error) {
            functions.logger.info("could not update profile with email address");
            functions.logger.error(error)
            responseCode = 404;
        }

        return corsMiddleWare(req, res, () => res.status(responseCode).send());
    })
})

export const createProfile = httpsFunctions.onRequest(async(req, res) => {
    return corsMiddleWare(req, res, async () => {
        const body: Basic = req.body;
        const summonerName = decodeURIComponent(body.summonerName);
        functions.logger.info(`Create profile request received for: ${summonerName}`);
        let responseCode = 200;

        functions.logger.info("Generate profile ID");
        const docId = encrypt(body.region, summonerName);

        if ((await FIRESTORE.collection(COLLECTIONS.profile).doc(docId).get()).exists) {
            functions.logger.info("Profile ID found in Database");
            functions.logger.info("User exists");
            responseCode = 202;
            functions.logger.info(`Summoner: ${summonerName} of ${body.region} found`);
            return corsMiddleWare(req, res, () => res.status(responseCode).send({
                region: body.region, summoner_name: summonerName
            }))
        }

        try {
            functions.logger.info("Attempt to add basic profile details to database");
            await FIRESTORE.collection(COLLECTIONS.profile).doc(docId).create({
                summonerName: summonerName,
                region: body.region
            });
            functions.logger.info(`Basic profile details with ID: ${docId} added to database`);
            responseCode = 201;
        } catch (error) {
            functions.logger.error("Basic profile details were not added to database");
            functions.logger.error(`Basic profile details failed to be added to database: ${error}`);   
            responseCode = 500;
        }

        return corsMiddleWare(req, res, () => res.status(responseCode).send(docId));
    })}
);

async function getSummoner(region: string, summonerName: string): Promise<GetSummonerByName | null> {
    functions.logger.info("Attempt to get summoner information");
    const url = `https://${region}.api.riotgames.com/lol/summoner/v4/summoners/by-name/${summonerName}`;
    let response: GetSummonerByName;

    const request = axios.request({
        url: url,
        headers: RIOT_HEADER
    })

    if ((await request).status !== 200) {
        functions.logger.error(((await request).data as Error).status.message);
        return null;
    };

    response = (await request).data as GetSummonerByName;
    return response;
}

async function getMatchList(region: string, accountId: string): Promise<{matches: MatchMetaData[]} | null> {
    functions.logger.info("Attempt to get match list info");
    const url = `https://${region}.api.riotgames.com/lol/match/v4/matchlists/by-account/${accountId}`;
    let response: {matches: MatchMetaData[]};

    const request = axios.request({
        url: url,
        headers: RIOT_HEADER
    })

    if ((await request).status !== 200) {
        functions.logger.error(JSON.stringify(((await request).data as Error).status.message));
        return null;
    };

    response = (await request).data as {matches: MatchMetaData[]};
    return response;
}

// async function getMatch(region: string, matchId: number): Promise<Match | null> {
//     const url = `https://${region}.api.riotgames.com/lol/match/v4/matches/${matchId}`;
//     let response: Match;

//     const request = axios.request({
//         url: url,
//         headers: RIOT_HEADER
//     })

//     if ((await request).status !== 200) {
//         functions.logger.error(JSON.stringify(((await request).data as Error).status.message));
//         return null;
//     };

//     response = (await request).data as Match;
//     return response;
// }

async function getLeagueEntries(region: string, summonerId: string): Promise<LeagueEntry[] | null> {
    functions.logger.info("Attempt to get league entry info");
    const url = `https://${region}.api.riotgames.com/lol/league/v4/entries/by-summoner/${summonerId}`;
    let response: LeagueEntry[];

    const request = axios.request({
        url: url,
        headers: RIOT_HEADER
    })

    if ((await request).status !== 200) {
        functions.logger.error(JSON.stringify(((await request).data as Error).status.message));
        return null;
    };

    response = (await request).data as LeagueEntry[];
    return response;
};

// async function getLeague(region: string, leagueId: string): Promise<League | null> {
//     const url = `https://${region}.api.riotgames.com/lol/league/v4/leagues/${leagueId}`;
//     let response: League;

//     const request = axios.request({
//         url: url,
//         headers: RIOT_HEADER
//     })

//     if ((await request).status !== 200) {
//         functions.logger.error(JSON.stringify(((await request).data as Error).status.message));
//         return null;
//     };

//     response = (await request).data as League;
//     return response;
// }

async function getChampionMastery(region: string, summonerId: string): Promise<ChampionMastery[] | null> {
    functions.logger.info("Attempt to get champion mastery info");
    const url = `https://${region}.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/${summonerId}`;
    let response: ChampionMastery[];

    const request = axios.request({
        url: url,
        headers: RIOT_HEADER
    })

    if ((await request).status !== 200) {
        functions.logger.error(JSON.stringify(((await request).data as Error).status.message));
        return null;
    };

    response = (await request).data as ChampionMastery[];
    return response;
};

// export const getProfileIcon = httpsFunctions.onRequest(async(req, res) => {
//     return corsMiddleWare(req, res, async () => {
//         const url = `http://ddragon.leagueoflegends.com/cdn/${CURRENT_VERSION}/img/profileicon/${profileIconId}.png`;

//         return corsMiddleWare(req, res, () => res.status(200).send(url));
//     })}
// );

function awawrdChampionTrophy(value: number) {
    return value > 10 ? "Champion Trophy" : "No Champion Trophy"
}

function awardLeagueTrophy(value: LeagueEntry) {
    const trophies: string[] = [];

    if (value.veteran) trophies.push("veteran");
    if (value.inactive) trophies.push("inactive");
    if (value.hotStreak) trophies.push("hot streak");
    if (value.freshBlood) trophies.push("fresh blood");
    if (value.leaguePoints >= 90) trophies.push("one more game!");
    if ((value.wins/value.losses)*100 < 50) trophies.push("loosing bum");
    if ((value.wins/value.losses)*100 > 60) trophies.push("hot damn");

    return trophies;
}

const TEIR_EMBLEMS: {[key: string]: string} = {
    "IRON": "Emblem_Iron",
    "BRONZE": "Emblem_Bronze",
    "SILVER": "Emblem_Silver",
    "GOLD": "Emblem_Gold",
    "PLATINUM": "Emblem_Platinum",
    "DIAMOND": "Emblem_Diamond",
    "MASTER": "Emblem_Master",
    "GRANDMASTER": "Emblem_Grandmaster",
    "CHALLENGER": "Emblem_Challenger"
};

async function getChampionUrl(championId: number): Promise<string> {
    functions.logger.info("Get Champion URL");
    const request = axios.request({
        url: CHAMPION_DATA_URL
    })
    const response = (await request).data as {data: {[key: string]: {id: string, key: string, name: string}}};

    let championName = ""
    Object.keys(response.data).map((value: string) => {
        functions.logger.debug(value)
        const currentChampionId = +response.data[value].key
        if (championId === currentChampionId) championName = value
    })

    functions.logger.info(championName + " is the champion we are looking for");
    return `http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${championName}_0.jpg`
}

export const onCreateProfile = triggerFunctions.document(`${COLLECTIONS.profile}/{profileId}`).onCreate(async (snap, context) => {
    const data = snap.data() as Basic;
    const {profileId} = context.params;

    const summoner = await getSummoner(data.region, encodeURIComponent(data.summonerName));
    let queueRef = [];
    let firestoreRef: any[] = [];
    let highestChampionMasteryPoints: number = 0;
    let championId = 0;
    let championsPlayed = 0;
    let trophies: string[] = [];
    
    if (!!summoner) {
        queueRef.push(QUEUE.ref(profileId).set({
            currentVersion: CURRENT_VERSION,
            summonerName: summoner.name,
            summonerLevel: summoner.summonerLevel,
            profileIconId: summoner.profileIconId
        }));

        const mastery = getChampionMastery(data.region, summoner.id);
        const entry = getLeagueEntries(data.region, summoner.id);
        const list = getMatchList(data.region, summoner.accountId);

        (await mastery)?.forEach(async (value: ChampionMastery, index: number) => {
            firestoreRef.push(FIRESTORE.collection(COLLECTIONS.mastery).doc().create({
                [index]: value,
                profilRef: profileId
            }));

            championsPlayed++;

            [highestChampionMasteryPoints, championId] =
                value.championPoints >= highestChampionMasteryPoints
                ? [value.championPoints, value.championId]
                : [highestChampionMasteryPoints, championId];
        })

        queueRef.push(QUEUE.ref(profileId).update({
            mostPlayedChampionUrl: await getChampionUrl(championId)
        }));

        (await entry)?.forEach((value: LeagueEntry, index: number) => {
            firestoreRef.push(FIRESTORE.collection(COLLECTIONS.league).doc().create({
                [index]: value,
                profileRef: profileId
            }))

            if (value.queueType === "RANKED_SOLO_5x5") {
                trophies.push(awawrdChampionTrophy(championsPlayed), ...awardLeagueTrophy(value));

                queueRef.push(QUEUE.ref(profileId).update({
                    rank: value.rank,
                    tier: value.tier,
                    emblemUrl: TEIR_EMBLEMS[value.tier],
                    leaguePoints: value.leaguePoints,
                    wins: value.wins,
                    losses: value.losses,
                    veteran: value.veteran,
                    inactive: value.inactive,
                    freshBlood: value.freshBlood,
                    hotStreak: value.hotStreak,
                    trophiesEarned: trophies
                }));
            }
            // if (RANKED_SOLO_5x5)
        });

        (await list)?.matches.forEach((value: MatchMetaData, index: number) => {
            firestoreRef.push(FIRESTORE.collection(COLLECTIONS.match).doc().create({
                [index]: value,
                profileRef: profileId
            }))
        });

        for(const ref of queueRef) {
            await ref;
        }

        for (const ref of firestoreRef) {
            await ref;
        }
    }
    
    functions.logger.error("Summoner is null");
    functions.logger.error("Cannot build out profile");
    const [region, summonerName] = deencrypt(profileId).split("|+|");
    await QUEUE.ref(profileId).set({
        currentVersion: CURRENT_VERSION,
        summonerName: summonerName,
        region: region
    })
})

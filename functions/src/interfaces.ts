export interface Ban {
    "championId": 9,
    "pickTurn": 1
};

export interface Team {
    "teamId": 100,
    "win": "Win",
    "firstBlood": false,
    "firstTower": false,
    "firstInhibitor": true,
    "firstBaron": false,
    "firstDragon": false,
    "firstRiftHerald": false,
    "towerKills": 11,
    "inhibitorKills": 6,
    "baronKills": 0,
    "dragonKills": 4,
    "vilemawKills": 0,
    "riftHeraldKills": 0,
    "dominionVictoryScore": 0,
    "bans": Ban[]
};

export interface Player {
    "platformId": "EUW1",
    "accountId": "hJzmDqbOFtYGnegXqt8JCjXvceEeUxkmlcy5vyamPuXaoss",
    "summonerName": "ChusSnow",
    "summonerId": "wHrLs9iXsIkkpurVR-r0SvSZ30GbMhDV2nHLA_Qanj0HFGxX",
    "currentPlatformId": "EUW1",
    "currentAccountId": "hJzmDqbOFtYGnegXqt8JCjXvceEeUxkmlcy5vyamPuXaoss",
    "matchHistoryUri": "/v1/stats/player_history/EUW1/235063924",
    "profileIcon": 4027
};

export interface ParticipantId {
    "participantId": 1,
    "player": Player
};

export interface Stats {
    "participantId": 1,
    "win": true,
    "item0": 3857,
    "item1": 3142,
    "item2": 3026,
    "item3": 3147,
    "item4": 3117,
    "item5": 3044,
    "item6": 3340,
    "kills": 6,
    "deaths": 7,
    "assists": 17,
    "largestKillingSpree": 4,
    "largestMultiKill": 1,
    "killingSprees": 1,
    "longestTimeSpentLiving": 703,
    "doubleKills": 0,
    "tripleKills": 0,
    "quadraKills": 0,
    "pentaKills": 0,
    "unrealKills": 0,
    "totalDamageDealt": 45571,
    "magicDamageDealt": 454,
    "physicalDamageDealt": 37347,
    "trueDamageDealt": 7770,
    "largestCriticalStrike": 0,
    "totalDamageDealtToChampions": 13039,
    "magicDamageDealtToChampions": 223,
    "physicalDamageDealtToChampions": 11677,
    "trueDamageDealtToChampions": 1138,
    "totalHeal": 6221,
    "totalUnitsHealed": 1,
    "damageSelfMitigated": 20847,
    "damageDealtToObjectives": 7592,
    "damageDealtToTurrets": 4676,
    "visionScore": 49,
    "timeCCingOthers": 38,
    "totalDamageTaken": 23850,
    "magicalDamageTaken": 7034,
    "physicalDamageTaken": 15664,
    "trueDamageTaken": 1152,
    "goldEarned": 13139,
    "goldSpent": 11350,
    "turretKills": 3,
    "inhibitorKills": 1,
    "totalMinionsKilled": 38,
    "neutralMinionsKilled": 6,
    "neutralMinionsKilledTeamJungle": 0,
    "neutralMinionsKilledEnemyJungle": 2,
    "totalTimeCrowdControlDealt": 135,
    "champLevel": 16,
    "visionWardsBoughtInGame": 0,
    "sightWardsBoughtInGame": 0,
    "wardsPlaced": 28,
    "wardsKilled": 0,
    "firstBloodKill": false,
    "firstBloodAssist": false,
    "firstTowerKill": false,
    "firstTowerAssist": false,
    "firstInhibitorKill": false,
    "firstInhibitorAssist": true,
    "combatPlayerScore": 0,
    "objectivePlayerScore": 0,
    "totalPlayerScore": 0,
    "totalScoreRank": 0,
    "playerScore0": 0,
    "playerScore1": 0,
    "playerScore2": 0,
    "playerScore3": 0,
    "playerScore4": 0,
    "playerScore5": 0,
    "playerScore6": 0,
    "playerScore7": 0,
    "playerScore8": 0,
    "playerScore9": 0,
    "perk0": 8439,
    "perk0Var1": 471,
    "perk0Var2": 0,
    "perk0Var3": 0,
    "perk1": 8446,
    "perk1Var1": 1213,
    "perk1Var2": 0,
    "perk1Var3": 0,
    "perk2": 8473,
    "perk2Var1": 1018,
    "perk2Var2": 0,
    "perk2Var3": 0,
    "perk3": 8453,
    "perk3Var1": 499,
    "perk3Var2": 0,
    "perk3Var3": 0,
    "perk4": 8143,
    "perk4Var1": 420,
    "perk4Var2": 0,
    "perk4Var3": 0,
    "perk5": 8106,
    "perk5Var1": 5,
    "perk5Var2": 0,
    "perk5Var3": 0,
    "perkPrimaryStyle": 8400,
    "perkSubStyle": 8100,
    "statPerk0": 5007,
    "statPerk1": 5002,
    "statPerk2": 5002
};

export interface Timeline {
    "participantId": 1,
    "creepsPerMinDeltas": {
        "10-20": 1.6,
        "0-10": 1.6,
        "30-end": 0,
        "20-30": 0.6
    },
    "xpPerMinDeltas": {
        "10-20": 374.79999999999995,
        "0-10": 298,
        "30-end": 523.6,
        "20-30": 529.3
    },
    "goldPerMinDeltas": {
        "10-20": 355.20000000000005,
        "0-10": 209,
        "30-end": 474.4,
        "20-30": 443.9
    },
    "csDiffPerMinDeltas": {
        "10-20": 0.9000000000000001,
        "0-10": -0.5499999999999998,
        "30-end": -2.4,
        "20-30": -1.55
    },
    "xpDiffPerMinDeltas": {
        "10-20": 43.54999999999998,
        "0-10": -7.799999999999997,
        "30-end": -201.79999999999995,
        "20-30": -52.349999999999994
    },
    "damageTakenPerMinDeltas": {
        "10-20": 837.5,
        "0-10": 361,
        "30-end": 435.8,
        "20-30": 773.2
    },
    "damageTakenDiffPerMinDeltas": {
        "10-20": 187.94999999999996,
        "0-10": 3.3500000000000227,
        "30-end": -71.09999999999991,
        "20-30": -184.3
    },
    "role": "DUO_SUPPORT",
    "lane": "BOTTOM"
};

export interface Participant {
    "participantId": 1,
    "teamId": 100,
    "championId": 555,
    "spell1Id": 12,
    "spell2Id": 4,
    "stats": Stats,
    "timeline": Timeline
};

export interface Match {
    gameId: number,
    platformId: string,
    gameCreation: number,
    gameDuration: number,
    queueId: number,
    mapId: number,
    seasonId: number,
    gameVersion: string,
    gameMode: string,
    gameType: string,
    teams: Team[],
    participants: Participant[],
    participantIdentities: ParticipantId[]
};

export interface LeagueEntry {
    leagueId: string,
    queueType: string,
    tier: string,
    rank: string,
    leaguePoints: number, //SEND message - in conjunction with inactive or lastplayed
    wins: number,
    losses: number,
    veteran: boolean, //BADGE/Trophy
    inactive: boolean, //SLEEPER ACCOUNT - send email - encourage gameplay
    freshBlood: boolean, //BADGE
    hotStreak: boolean //TROPHY - but better defined via match history
};

export interface ChampionMastery {
    championId: number,
    championLevel: number,
    championPoints: number,
    lastPlayTime: number,
    championPointsSinceLastLevel: number,
    championPointsUntilNextLevel: number,
    chestGranted: boolean,
    tokensEarned: number
};

export interface MatchMetaData {
    platformId: string,
    gameId: number,
    champion: number,
    queue: number,
    season: number,
    timestamp: number,
    role: string
    lane: string
}

export interface Error {
    status: {
        message: string,
        status_code: number
    }
}

export interface GetSummonerByName {
    id: string,
    accountId: string,
    name: string,
    profileIconId: number,
    revisionDate: number,
    summonerLevel: number
}


export interface Basic {
    summonerName: string,
    region: string
}

interface Entry {
    summonerId: string,
    summonerName: string,
    leaguePoints: number,
    rank: string,
    wins: number,
    losses: number,
    veteran: boolean,
    inactive: boolean,
    freshBlood: boolean,
    hotStreak: boolean
};

export interface League {
    tier: string,
    leagueId: string,
    queue: string,
    name: string,
    entries: Entry[]
}